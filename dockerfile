FROM alpine:3.10

# This is the release of Vault to pull in.
ARG VAULT_VERSION=1.2.0-beta2

# Create a app user and group first so the IDs get set the same way,
# even as the rest of this may change over time.
RUN addgroup app && \
    adduser -S -G app app -u 10005

# Set up certificates, our base tools, and Vault.
RUN apk add --update --no-cache ca-certificates sudo && \
    echo "app ALL=(root) NOPASSWD:ALL" >> /etc/sudoers

# Execute everythin under user app
USER app
