# IX Base container

Alpine-based container with the following additions:

- User app with ID 10005
- User app is member of sudoers without Password
- Application sudo, ca-certificates installed

## Example Deployment

Create Deployment YAML

```bash
cat <<EOF > ix.yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: null
  generation: 1
  labels:
    run: ix
  name: ix
  namespace: ix
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      run: ix
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        run: ix
    spec:
      containers:
      - command: ["/bin/sh"]
        args: ["-c", "while true; do touch /tmp/healthy; sleep 600; rm -rf /tmp/healthy; sleep 30;done"]
        livenessProbe:
          initialDelaySeconds: 5
          periodSeconds: 5
          exec:
            command:
            - cat
            - /tmp/healthy
        #   httpGet:
        #     path: /healthz
        #     port: 8080
        #     httpHeaders:
        #     - name: Custom-Header
        #       value: Awesome
        image: tribock/ix
        imagePullPolicy: Always
        name: ix
        resources:
          limits:
            cpu: 500m
            memory: 1024Mi
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status: {}
EOF
```

deploy

```bash
kubectl create ns ix
kubectl create -f ix.yaml
```

## Write Docker File

To write your own dockerfile just start with:

```Dockerfile
FROM tribock/ix
```